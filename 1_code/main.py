﻿# Tesonet - Senior Python Developer - Task Report
# by Aleksandras Urbonas, Vilnius, 2018

# 1. open the target_page

# python's internal web browser opener
import webbrowser

# target page for search
target_page = 'https://www.owler.com/'

# list of companies of interest
companies = ['microsoft','apple','google','sony','dell','amazon','samsung','bank-of-america','huawei','verizon','novartis','statefarm','walmart','comcast','vodafone','toyota','volkswagenag','airbus-commercial','honda','nissan-global','costco','valero','hyundai','home-depot','total','audi','uhc','unitedhealth-group','delta','antheminc']

# iterate list of companies
for company in companies:
	# specify an individual link for each company
	target_link = target_page + 'company/' + company
	# open brower to a specified link
	webbrowser.open(target_link)

## Others
# 2. enter first company name or domain to search

# 3. click `Search`

# 4. save results page as `YYYYMMDD_company_results.html`
