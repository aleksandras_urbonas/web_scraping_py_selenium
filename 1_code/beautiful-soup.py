﻿# Tesonet - Senior Python Developer - Task Report
#   by Aleksandras Urbonas, Vilnius, 2018

import bs4

# list of companies of interest
companies = ['microsoft'] #,'apple','google','sony','dell','amazon','samsung','bank-of-america','huawei','verizon','novartis','statefarm','walmart','comcast','vodafone','toyota','volkswagenag','airbus-commercial','honda','nissan-global','costco','valero','hyundai','home-depot','total','audi','uhc','unitedhealth-group','delta','antheminc']

# 1. open the download file

# iterate list of companies
for company in companies:

	# check company name
	print(company)

	# specify an individual link for each company
	collected_html = open('collected_html/' + company + '.htm')

	# convert html to beautiful soup
	html_soup = bs4.BeautifulSoup(collected_html, features="html.parser")

	# check type
	#print(type(html_soup))

	# tags of interest
	tag_CEO_NAME = 'p[class="name cp-page"]'
	tag_CEO_RATING = 'h2[class="ceo-rating-text cp-page title black"] > span'
	tag_EMPLOYEES = 'h2[class="botifyemployeedata title black"] > span[class=""]'
	tag_ANNUAL_REVENUE = 'h2[class="botifyrevenuedata title black"] > span[class=""]'

	tags = [tag_CEO_NAME, tag_CEO_RATING, tag_EMPLOYEES, tag_ANNUAL_REVENUE]
	
	# file for results
	results_file_name = 'results_' + company + '.txt'
	results = open(results_file_name, 'w')

	for tag in tags:
		# find element by specified tag, single element
		one_elem = html_soup.select(tag)[0]
		# get text value of the found element
		value = one_elem.getText().rstrip()
		# output text
		# print(value, '\n')
		
		# write to file
		results.write("%s\t" %value)

	# print('Results', result)

print('The end.')
