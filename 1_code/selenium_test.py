﻿# Tesonet - Senior Python Developer - Task Report
# by Aleksandras Urbonas, Vilnius, 2018

# 1. open the target_page

# 
from selenium import webdriver

# launch browser
browser = webdriver.Firefox()
type(browser)

# target page for search
url = 'https://www.owler.com/'

# list of companies of interest
companies = ['microsoft']#,'apple','google','sony','dell','amazon','samsung','bank-of-america','huawei','verizon','novartis','statefarm','walmart','comcast','vodafone','toyota','volkswagenag','airbus-commercial','honda','nissan-global','costco','valero','hyundai','home-depot','total','audi','uhc','unitedhealth-group','delta','antheminc']

# iterate list of companies
for company in companies:
	# check company name
	print(company)
	# specify an individual link for each company
	company_url = url + 'company/' + company
	
	# request
	browser.get(company_url)
