﻿# Tesonet - Senior Python Developer - Task Report
# by Aleksandras Urbonas, Vilnius, 2018

# 1. open the target_page

# 
import requests

# target page for search
url = 'https://www.owler.com/'

# list of companies of interest
companies = ['microsoft']#,'apple','google','sony','dell','amazon','samsung','bank-of-america','huawei','verizon','novartis','statefarm','walmart','comcast','vodafone','toyota','volkswagenag','airbus-commercial','honda','nissan-global','costco','valero','hyundai','home-depot','total','audi','uhc','unitedhealth-group','delta','antheminc']

# iterate list of companies
for company in companies:
	# check company name
	print(company)
	# specify an individual link for each company
	company_url = url + 'company/' + company
	
	# request data using `get`
	res = requests.get(company_url)

	# try avoid error
	try:
		# raise error on bad download
		res.raise_for_status()
	except Exception as exception:
		#print(type(res))
		print(res.status_code)
		# 405 = method not allowed
		print('There was an exception: %s ' % (exception))

# upon download
#	if res.status_code == requests.codes.ok:

	"""Output some basic info about the text."""
	print(len(res.text))
	print(res.text[:100])
	
	# open file handler in write binary mode
	saveFile = open('data_' + company + '.txt', 'wb')
	# write data
	for chunk in res.iter_content(100000):
		saveFile.write(chunk)
	
	# close the handler
	saveFile.close()
