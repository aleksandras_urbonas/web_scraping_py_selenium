#! python3
# mapIt.py - launches a map in the browser using an address from command line or clipboard.

# webbrowser: open browser
# sys: command line arguments
# pyperclip: use clipboard

import webbrowser, sys, pyperclip

if len(sys.argv) > 1:
	# Get address from the command line.
	address = ' '.join(sys.argv[1:])
	# Command line: 870 Valencia St, San Francisco, CA 94110

	# QC: Check arguments from command line.
	# print(sys.argv)
else:
	# Get address from clipboard.
	# Error, commented, testing
	address = pyperclip.paste()
	# Clipboard: Mindaugo 23, Vilnius, LT 03214


# Launch the browser and open the specified address.
webbrowser.open(
	'https://www.google.com/maps/place/' + address
)